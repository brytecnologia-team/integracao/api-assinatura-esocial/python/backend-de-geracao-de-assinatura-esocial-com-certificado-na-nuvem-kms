from flask import Flask, request
from flask_restful import Resource, Api
from flask_cors import CORS
import requests

app = Flask(__name__)
CORS(app)
api = Api(app)

# Token de autenticação gerado no BRy Cloud
AUTHORIZATION = ''

# Chave de autorização (PIN) do usuário signatário, codificada em BASE64
KMS_CREDENCIAL = ''

# Tipo de credencial do KMS, "PIN" ou "TOKEN"
KMS_CREDENCIAL_TIPO = 'PIN'

#URL do serviço de assinatura digital via KMS
#URL_ASSINATURA = 'https://hub2.bry.com.br/api/xml-signature-service/v1/signatures/kms'
URL_ASSINATURA = 'https://hub2.hom.bry.com.br/api/xml-signature-service/v1/signatures/kms'


class assina_KMS(Resource):
    def post(self):
        # Pega o conteúdo que vem da requisição do front-end
        data = request.form

        # Lê o documento XML enviado para assinatura no front-end
        xml_eSocial = request.files['documento'].read()

        # Pega o uuid do certificado que realizará assinatura
        uuid = data['uuid']

        # Dados que definem os parâmetros utilizados na assinatura eSocial
        signer_form = {
            'nonce': 1, 
            'signatureFormat': 'ENVELOPED',
            'hashAlgorithm': 'SHA256',
            'profile': 'BASIC',
            'returnType':'LINK',
            'canonicalizerType':'INCLUSIVE',
            'generateSimplifiedXMLDSig':'true',
            'includeXPathEnveloped':'false',
            'originalDocuments[0][nonce]': '1',
            'uuid' : uuid
        }

        files = []
        files.append(('originalDocuments[0][content]', xml_eSocial))

        header = {
            'Authorization': AUTHORIZATION,
            'kms_credencial': KMS_CREDENCIAL,
            'kms_credencial_tipo': KMS_CREDENCIAL_TIPO
        }

        print('============= Iniciando assinatura no BRy HUB utilizando certificado em nuvem ... =============')

        response = requests.post(URL_ASSINATURA,
                                 data=signer_form, files=files, headers=header)
        if response.status_code == 200:
            print('Assinatura realizada com sucesso!')
            print(response.json())

            return response.json()['documentos'][0]['links'][0]['href']
        else:
            print(response.text)


api.add_resource(assina_KMS, '/assinarKMS')

if __name__ == '__main__':
    app.run(host="localhost", port=5000, debug=True)
